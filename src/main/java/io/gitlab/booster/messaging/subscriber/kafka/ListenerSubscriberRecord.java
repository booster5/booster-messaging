package io.gitlab.booster.messaging.subscriber.kafka;

import lombok.NonNull;
import org.springframework.kafka.support.Acknowledgment;

/**
 * a record to be acknowledged from kafka listener
 * @param <T> type of data
 */
public class ListenerSubscriberRecord<T> extends SubscriberRecord<T> {

    /**
     * Acknowledgement object, cannot be null.
     */
    @NonNull
    private final Acknowledgment acknowledgment;

    /**
     * Constructor
     * @param data data to be consumed
     * @param acknowledgment {@link Acknowledgment} object
     */
    public ListenerSubscriberRecord(@NonNull T data, @NonNull Acknowledgment acknowledgment) {
        super(data);
        this.acknowledgment = acknowledgment;
    }

    @Override
    public void acknowledge() {
        this.acknowledgment.acknowledge();
    }
}
