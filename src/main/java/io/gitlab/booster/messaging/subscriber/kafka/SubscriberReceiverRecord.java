package io.gitlab.booster.messaging.subscriber.kafka;

import lombok.NonNull;
import reactor.kafka.receiver.ReceiverRecord;

/**
 * A {@link SubscriberRecord} from {@link ReceiverRecord}
 * @param <T> type of data
 */
public class SubscriberReceiverRecord<T> extends SubscriberRecord<T> {

    private final ReceiverRecord<String, T> record;

    /**
     * Constructor from {@link ReceiverRecord}
     * @param record {@link ReceiverRecord}
     */
    public SubscriberReceiverRecord(@NonNull ReceiverRecord<String, T> record) {
        super(record.value());
        this.record = record;
    }

    @Override
    public void acknowledge() {
        this.record.receiverOffset().acknowledge();
    }
}
