package io.gitlab.booster.messaging.subscriber.kafka;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import org.springframework.kafka.support.Acknowledgment;
import reactor.kafka.receiver.ReceiverRecord;

/**
 * A record is an object consumed by Kafka consumer
 * Booster messaging assumes the records are not
 * auto-acknowledged.
 *
 * @param <T> type of subscriber record.
 */
@Getter
@EqualsAndHashCode
@ToString
public abstract class SubscriberRecord<T> {

    /**
     * Data received from Kafka, cannot be null
     */
    @NonNull
    private final T data;

    /**
     * Constructs a {@link SubscriberRecord}
     * @param data message received
     */
    public SubscriberRecord(
            @NonNull T data
    ) {
        this.data = data;
    }

    /**
     * Acknowledges message
     */
    abstract public void acknowledge();

    /**
     * Creates a listener record
     * @param data data contained in the record
     * @param acknowledgment {@link Acknowledgment} object to ack the message
     * @return returns {@link SubscriberRecord}
     * @param <T> type of data
     */
    public static <T> SubscriberRecord<T> create(T data, Acknowledgment acknowledgment) {
        return new ListenerSubscriberRecord<>(data, acknowledgment);
    }

    /**
     * Creates a reactive kafka record
     * @param record data {@link ReceiverRecord}
     * @return returns {@link SubscriberRecord}
     * @param <T> type of data
     */
    public static <T> SubscriberRecord<T> create(ReceiverRecord<String, T> record) {
        return new SubscriberReceiverRecord<>(record);
    }
}
