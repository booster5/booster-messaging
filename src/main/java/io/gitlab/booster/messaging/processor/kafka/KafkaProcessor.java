package io.gitlab.booster.messaging.processor.kafka;

import io.gitlab.booster.commons.metrics.MetricsRegistry;
import io.gitlab.booster.messaging.MessagingMetricsConstants;
import io.gitlab.booster.messaging.config.OpenTelemetryConfig;
import io.gitlab.booster.messaging.processor.AbstractProcessor;
import io.gitlab.booster.messaging.subscriber.SubscriberFlow;
import io.gitlab.booster.messaging.subscriber.kafka.SubscriberRecord;
import io.gitlab.booster.task.Task;
import io.opentelemetry.api.trace.Span;
import io.opentelemetry.context.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Kafka processor to process Kafka events coming from {@link SubscriberFlow}
 * @param <T> type of Kafka payload.
 */
public class KafkaProcessor<T> extends AbstractProcessor<SubscriberRecord<T>> {

    private final static Logger log = LoggerFactory.getLogger(KafkaProcessor.class);

    /**
     * Constructs a {@link KafkaProcessor}
     * @param subscriberFlow {@link SubscriberFlow} to listen to.
     * @param processTask {@link Task} used to process Kafka events
     * @param openTelemetryConfig {@link OpenTelemetryConfig} for trace
     * @param registry metrics recording.
     * @param manuallyInjectTrace whether to manually inject trace or leave it to OTEL instrumentation
     */
    public KafkaProcessor(
            SubscriberFlow<SubscriberRecord<T>> subscriberFlow,
            Task<SubscriberRecord<T>, SubscriberRecord<T>> processTask,
            OpenTelemetryConfig openTelemetryConfig,
            MetricsRegistry registry,
            boolean manuallyInjectTrace
    ) {
        super(
                MessagingMetricsConstants.KAFKA,
                subscriberFlow,
                processTask,
                openTelemetryConfig,
                registry,
                manuallyInjectTrace
        );
    }

    /**
     * Acknowledges Kafka events once successfully processed.
     * @param record record to be acknowledged.
     * @return true if successfully acknowledged, false otherwise.
     */
    @Override
    protected boolean acknowledge(SubscriberRecord<T> record) {
        if (record != null) {
            log.debug("booster-messaging - acknowledging message in processor[{}], record: {}", this.getName(), record);
            try {
                record.acknowledge();
                return true;
            } catch (Throwable t) {
                log.error("booster-messaging - processor[{}] acknowledgement caused exception, record: {}", this.getName(), record, t);
                return false;
            }
        } else {
            log.debug("booster-messaging - no message to be acknowledged in processor[{}]", this.getName());
            return false;
        }
    }

    @Override
    protected Context createContext(SubscriberRecord<T> record) {
        return Context.current();
    }

    @Override
    protected Span createSpan(Context context) {
        return Span.current();
    }
}
