package io.gitlab.booster.messaging.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import reactor.core.publisher.Mono;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * {@link CompletableFuture} utility methods
 */
public interface FutureHelper {

    /**
     * {@link Logger} to generate logs.
     */
    Logger log = LoggerFactory.getLogger(FutureHelper.class);

    /**
     * Retrieves value from {@link CompletableFuture}
     * @param future {@link CompletableFuture} where execution result is expected.
     * @return true if success, false if error encountered
     * @param <T> type of {@link CompletableFuture}
     */
    static <T> boolean fromCompletableFutureToBoolean(CompletableFuture<T> future) {

        AtomicBoolean result = new AtomicBoolean(true);

        CountDownLatch latch = new CountDownLatch(1);
        future.whenCompleteAsync((v, t) -> {
            if (t != null) {
                log.error("booster-messaging - error in completable future", t);
                result.set(false);
            } else {
                log.debug("booster-messaging - completable future success: {}", v);
                result.set(true);
            }
            latch.countDown();
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            log.error("booster-messaging - completable future await exception", e);
            return false;
        }
        return result.get();
    }
}
