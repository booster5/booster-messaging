package io.gitlab.booster.messaging.subscriber.kafka;

import org.junit.jupiter.api.Test;
import org.springframework.kafka.support.Acknowledgment;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

class SubscriberRecordTest {

    @Test
    void shouldFail() {
        assertThrows(
                NullPointerException.class,
                () -> SubscriberRecord.create(null, mock(Acknowledgment.class))
        );
        assertThrows(
                NullPointerException.class,
                () -> SubscriberRecord.create(1, null)
        );
        assertThrows(
                NullPointerException.class,
                () -> SubscriberRecord.create(null, null)
        );
    }

    @Test
    void shouldCreate() {
        assertThat(
                SubscriberRecord.create(1, mock(Acknowledgment.class)),
                notNullValue()
        );
    }
}
