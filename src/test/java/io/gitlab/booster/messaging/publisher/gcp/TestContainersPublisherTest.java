package io.gitlab.booster.messaging.publisher.gcp;

import arrow.core.Either;
import arrow.core.Option;
import com.google.api.gax.core.NoCredentialsProvider;
import com.google.api.gax.grpc.GrpcTransportChannel;
import com.google.api.gax.rpc.FixedTransportChannelProvider;
import com.google.api.gax.rpc.TransportChannelProvider;
import com.google.cloud.spring.core.GcpProjectIdProvider;
import com.google.cloud.spring.pubsub.core.PubSubConfiguration;
import com.google.cloud.spring.pubsub.core.publisher.PubSubPublisherTemplate;
import com.google.cloud.spring.pubsub.core.subscriber.PubSubSubscriberTemplate;
import com.google.cloud.spring.pubsub.support.AcknowledgeablePubsubMessage;
import com.google.cloud.spring.pubsub.support.DefaultPublisherFactory;
import com.google.cloud.spring.pubsub.support.DefaultSubscriberFactory;
import io.gitlab.booster.commons.metrics.MetricsRegistry;
import io.gitlab.booster.config.thread.ThreadPoolConfig;
import io.gitlab.booster.config.thread.ThreadPoolSetting;
import io.gitlab.booster.messaging.config.BoosterMessagingConfig;
import io.gitlab.booster.messaging.config.GcpPubSubSubscriberConfig;
import io.gitlab.booster.messaging.config.GcpPubSubSubscriberSetting;
import io.gitlab.booster.messaging.config.OpenTelemetryConfig;
import io.gitlab.booster.messaging.publisher.PublisherRecord;
import io.gitlab.booster.messaging.subscriber.gcp.GcpPubSubPullSubscriber;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.opentelemetry.api.OpenTelemetry;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PubSubEmulatorContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;

import static io.gitlab.booster.messaging.util.gcp.GcpUtil.createSubscription;
import static io.gitlab.booster.messaging.util.gcp.GcpUtil.createTopic;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {BoosterMessagingConfig.class})
@Testcontainers
class TestContainersPublisherTest {

    public static final String PROJECT_ID = "test-project";
    public static final String TOPIC_ID = "test-topic";
    public static final String SUBSCRIPTION_ID = "greetingSubscription";

    private final NoCredentialsProvider credentialsProvider = NoCredentialsProvider.create();

    private final GcpProjectIdProvider projectIdProvider = () -> PROJECT_ID;

    @Autowired
    private ConfigurableApplicationContext applicationContext;

    private PubSubPublisherTemplate publisherTemplate;

    private PubSubSubscriberTemplate subscriberTemplate;

    @Container
    public PubSubEmulatorContainer pubSubEmulatorContainer = new PubSubEmulatorContainer(
            DockerImageName.parse("gcr.io/google.com/cloudsdktool/google-cloud-cli:441.0.0-emulators")
    );

    @BeforeEach
    public void setup() throws IOException {
        String hostPort = this.pubSubEmulatorContainer.getEmulatorEndpoint();
        ManagedChannel channel = ManagedChannelBuilder.forTarget(hostPort).usePlaintext().build();
        TransportChannelProvider channelProvider = FixedTransportChannelProvider.create(GrpcTransportChannel.create(channel));

        DefaultPublisherFactory publisherFactory = new DefaultPublisherFactory(this.projectIdProvider);
        publisherFactory.setChannelProvider(channelProvider);
        publisherFactory.setCredentialsProvider(this.credentialsProvider);

        this.publisherTemplate = new PubSubPublisherTemplate(publisherFactory);

        PubSubConfiguration configuration = new PubSubConfiguration();
        configuration.initialize(PROJECT_ID);
        DefaultSubscriberFactory subscriberFactory = new DefaultSubscriberFactory(this.projectIdProvider, configuration);
        subscriberFactory.setChannelProvider(channelProvider);
        subscriberFactory.setCredentialsProvider(this.credentialsProvider);

        this.subscriberTemplate = new PubSubSubscriberTemplate(subscriberFactory);

        createTopic(PROJECT_ID, TOPIC_ID, channelProvider, this.credentialsProvider);
        createSubscription(PROJECT_ID, SUBSCRIPTION_ID, TOPIC_ID, channelProvider, this.credentialsProvider);
    }

    private String publish(String content) {
        try {
            return this.publisherTemplate.publish(TOPIC_ID, content).get();
        } catch (ExecutionException | InterruptedException e) {
            return null;
        }
    }

    @Test
    void shouldPublish() {
        assertThat(this.publish("greeting"), notNullValue());
    }

    @Test
    void shouldPull() {
        String recordId = this.publish("test");
        assertThat(recordId, notNullValue());

        List<AcknowledgeablePubsubMessage> messages =
                this.subscriberTemplate.pull(SUBSCRIPTION_ID, 10, true);
        assertThat(messages, hasSize(1));
        assertThat(messages.get(0).getPubsubMessage().getData().toString(StandardCharsets.UTF_8), equalTo("test"));
    }

    private Mono<Either<Throwable, Option<PublisherRecord>>> publishWithPublisher(String content) {
        ThreadPoolConfig threadPoolConfig = this.createThreadConfig();

        GcpPubSubPublisher<String> publisher = new GcpPubSubPublisher<>(
                "test",
                this.publisherTemplate,
                threadPoolConfig,
                new MetricsRegistry(new SimpleMeterRegistry()),
                new OpenTelemetryConfig(OpenTelemetry.noop(), "test"),
                false
        );

        return publisher.publish(TOPIC_ID, new PubsubRecord<>(content));
    }

    private ThreadPoolConfig createThreadConfig() {
        ThreadPoolConfig config = new ThreadPoolConfig(null, null);
        config.setSettings(Map.of("test", new ThreadPoolSetting()));
        return config;
    }

    private GcpPubSubSubscriberConfig createGcpPubSubSubscriberConfig() {
        GcpPubSubSubscriberConfig config = new GcpPubSubSubscriberConfig();
        GcpPubSubSubscriberSetting setting = new GcpPubSubSubscriberSetting();
        setting.setSubscription(SUBSCRIPTION_ID);
        setting.setMaxRecords(10);
        config.setSettings(Map.of("test", setting));
        return config;
    }

    @Test
    void shouldPublishAndReceive() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        StepVerifier.create(this.publishWithPublisher("test"))
                .consumeNextWith(result -> {
                    assertThat(result.isRight(), equalTo(true));
                    Option<PublisherRecord> recordOption = result.getOrNull();
                    assertThat(recordOption, notNullValue());
                    assertThat(recordOption.isDefined(), equalTo(true));
                    assertThat(recordOption.orNull(), notNullValue());
                    assertThat(recordOption.orNull().getRecordId(), notNullValue());
                    latch.countDown();
                }).verifyComplete();

        latch.await();

        ThreadPoolSetting setting = new ThreadPoolSetting();
        ThreadPoolConfig threadPoolConfig = new ThreadPoolConfig(this.applicationContext, new MetricsRegistry(new SimpleMeterRegistry()));
        threadPoolConfig.setSettings(Map.of("test", setting));

        GcpPubSubPullSubscriber subscriber = new GcpPubSubPullSubscriber(
                "test",
                this.subscriberTemplate,
                this.createThreadConfig(),
                this.createGcpPubSubSubscriberConfig(),
                new MetricsRegistry(new SimpleMeterRegistry()),
                new OpenTelemetryConfig(OpenTelemetry.noop(), "test"),
                false
        );

        StepVerifier.create(subscriber.flatFlux().take(1))
                .consumeNextWith(message -> {
                    assertThat(message.getPubsubMessage(), notNullValue());
                    assertThat(message.getPubsubMessage().getData().toString(StandardCharsets.UTF_8), equalTo("test"));
                }).verifyComplete();
    }
}
