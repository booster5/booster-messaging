package io.gitlab.booster.messaging.util.gcp;

import com.google.api.gax.core.CredentialsProvider;
import com.google.api.gax.rpc.TransportChannelProvider;
import com.google.cloud.pubsub.v1.SubscriptionAdminClient;
import com.google.cloud.pubsub.v1.SubscriptionAdminSettings;
import com.google.cloud.pubsub.v1.TopicAdminClient;
import com.google.cloud.pubsub.v1.TopicAdminSettings;
import com.google.pubsub.v1.PushConfig;
import com.google.pubsub.v1.SubscriptionName;
import com.google.pubsub.v1.TopicName;

import java.io.IOException;

public interface GcpUtil {

    static void createTopic(
            String projectId,
            String topicId,
            TransportChannelProvider channelProvider,
            CredentialsProvider credentialsProvider
    ) throws IOException {
        TopicAdminSettings topicAdminSettings = TopicAdminSettings
                .newBuilder()
                .setTransportChannelProvider(channelProvider)
                .setCredentialsProvider(credentialsProvider)
                .build();
        try (TopicAdminClient topicAdminClient = TopicAdminClient.create(topicAdminSettings)) {
            topicAdminClient.createTopic(TopicName.of(projectId, topicId));
        }
    }

    static void createSubscription(
            String projectId,
            String subscriptionId,
            String topicId,
            TransportChannelProvider channelProvider,
            CredentialsProvider credentialsProvider
    ) throws IOException {
        SubscriptionAdminSettings subscriptionAdminSettings = SubscriptionAdminSettings
                .newBuilder()
                .setTransportChannelProvider(channelProvider)
                .setCredentialsProvider(credentialsProvider)
                .build();
        try (SubscriptionAdminClient subscriptionAdminClient = SubscriptionAdminClient.create(subscriptionAdminSettings)) {
            SubscriptionName subscriptionName = SubscriptionName.of(projectId, subscriptionId);
            subscriptionAdminClient.createSubscription(
                    subscriptionName,
                    TopicName.of(projectId, topicId),
                    PushConfig.getDefaultInstance(),
                    10
            );
        }
    }
}
