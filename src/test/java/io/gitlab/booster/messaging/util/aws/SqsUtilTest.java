package io.gitlab.booster.messaging.util.aws;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

class SqsUtilTest {

    @Test
    void shouldGenerateUrl() {
        String url = "http://sqs.us-east-1.localhost:34688/000000000000/test-queue";
        String newUrl = SqsUtil.reformatUrl(url, 4096);
        assertThat(
                newUrl,
                equalTo("http://localhost:4096/000000000000/test-queue")
        );
    }
}
