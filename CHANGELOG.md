# Change Log

## v3.1.0
1. Update to Spring Boot 3.3.7
2. Update to Spring Cloud 2023.0.4
3. Update to Spring Cloud GCP 5.9.0

## v3.0.0
1. Change to Spring Boot 3.2.5

## v2.0.0
1. Repackaging.

## v1.0.0
1. Initial version
2. Uses
   1. Booster web starter 1.0.1
   2. Spring Boot 2.7.6
   3. Spring Cloud 2021.0.5
   4. Spring Cloud GCP 3.4.0
